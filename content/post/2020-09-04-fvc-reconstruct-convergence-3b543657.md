---
title: fvc-reconstruct commit 3b543657
description: 13:42:43 CEST
date: 2020-09-04 13:42:43 +0200
---

```python
from fvc_reconstruct import *
from pandas import read_csv
```


```python
analyze_test('hex2D/SHEAR_2D')
```


![png](/images/fvc-reconstruct-SHEAR-convergence-3b543657920a9705297fc1a72e0c87fc2e870470.png)



```python
analyze_test('hex2D/HADAMARD_RYBCZYNSKY_2D')
```


![png](/images/fvc-reconstruct-HADAMARD_RYBCZYNSKY-convergence-3b543657920a9705297fc1a72e0c87fc2e870470.png)



```python
%%python

import unittest
from pandas import read_csv

class TestNumbersMethods(unittest.TestCase):
    def test_SHEARD_2D_threshold(self):
        min_val : shear2D_data['O(L_INF)'].min()
        self.assertGreater(min_val, 1.8)
    #def test_HADAMARD_RYBCZYNSKY_2D_threshold(self):
    #    min_val : hd2D_data['O(L_INF)'].min()
    #    self.assertGreater(min_val, 1.8)

shear2D_data : read_csv('SHEAR_2D.csv', float_precision:'round_trip', comment:'#')
hd2D_data : read_csv('HADAMARD_RYBCZYNSKY_2D.csv', float_precision:'round_trip', comment:'#')

unittest.main(verbosity:2)
```

    test_SHEARD_2D_threshold (__main__.TestNumbersMethods) ... ok
    
    ----------------------------------------------------------------------
    Ran 1 test in 0.001s
    
    OK

